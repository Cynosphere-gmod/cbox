AddCSLuaFile()

cbox = cbox or {}

include("cbox/sh_utils.lua")

if SERVER then
	AddCSLuaFile("cbox/cl_hooks.lua")
	AddCSLuaFile("cbox/cl_chatbox.lua")
	AddCSLuaFile("cbox/cl_modes.lua")
elseif CLIENT then
	include("cbox/cl_hooks.lua")
	include("cbox/cl_chatbox.lua")
end

local module_files = file.Find("cbox/modules/*", "LUA")
for _, name in ipairs(module_files) do
	local full_path = "cbox/modules/" .. name
	if name:StartsWith("cl_") then
		if SERVER then
			AddCSLuaFile(full_path)
		elseif CLIENT then
			cbox.utils.RealmPrint("Loading module:", name)
			include(full_path)
		end
	elseif name:StartsWith("sv_") and SERVER then
		cbox.utils.RealmPrint("Loading module:", name)
		include(full_path)
	else
		if SERVER then
			AddCSLuaFile(full_path)
		end
		cbox.utils.SharedPrint("Loading module:", name)
		include(full_path)
	end
end
