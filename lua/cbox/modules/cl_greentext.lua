local ENABLED = CreateClientConVar("cbox_greentext", "1", true, false, "Enables greentext", 0, 1)
local COLOR = CreateClientConVar("cbox_greentext_color", "175 201 96", true, false, "Color greentext should be")

local color_white = Color(255, 255, 255)

cbox.hooks.Add("PreChatAddText", "cbox.greentext", function(args)
	if not ENABLED:GetBool() then return end

	for i, arg in ipairs(args) do
		local prevArg = args[i - 1]
		if isstring(prevArg) and prevArg == ": " and isstring(arg) and arg:StartsWith(">") then
			table.insert(args, i, cbox.utils.ParseColorStringToColor(COLOR:GetString()))
			break
		end
	end

	return args
end)
