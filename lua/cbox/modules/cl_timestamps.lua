local ENABLED = CreateClientConVar("cbox_timestamps", "1", true, false, "Enables timestamps", 0, 1)
local TRUETIME = CreateClientConVar("cbox_timestamps_24hr", "1", true, false, "Whether timestamps should be 24 hour instead of 12 hour", 0, 1)
local SECONDS = CreateClientConVar("cbox_timestamps_seconds", "0", true, false, "Whether timestamps should display seconds", 0, 1)
local COLOR = CreateClientConVar("cbox_timestamps_color", "151 211 255", true, false, "Color timestamps should be")
local SEPARATOR_COLOR = CreateClientConVar("cbox_timestamps_separator_color", "255 255 255", true, false, "Color for the timestamp separator")
local BRACKETS = CreateClientConVar("cbox_timestamps_brackets", "0", true, false, "Whether to replace the separator with surrounded brackets", 0, 1)
local ALL_MESSAGES = CreateClientConVar("cbox_timestamps_all", "1", true, false, "Apply timestamps to all messages or just players", 0, 1)

local color_white = Color(255, 255, 255)

local function MakeTimestamp(args)
	local use24 = TRUETIME:GetBool()
	local stamp = ""

	if use24 then
		stamp = stamp .. os.date("%H:")
	else
		stamp = stamp .. os.date("%I:")
	end

	stamp = stamp .. os.date("%M")

	if SECONDS:GetBool() then
		stamp = stamp .. os.date(":%S")
	end

	if not use24 then
		stamp = stamp .. os.date(" %p")
	end

	local new_args = {}

	if BRACKETS:GetBool() then
		new_args[#new_args + 1] = cbox.utils.ParseColorStringToColor(SEPARATOR_COLOR:GetString())
		new_args[#new_args + 1] = "["
	end

	new_args[#new_args + 1] = cbox.utils.ParseColorStringToColor(COLOR:GetString())
	new_args[#new_args + 1] = stamp

	new_args[#new_args + 1] = cbox.utils.ParseColorStringToColor(SEPARATOR_COLOR:GetString())
	if BRACKETS:GetBool() then
		new_args[#new_args + 1] = "] "
	else
		new_args[#new_args + 1] = " - "
	end

	for _, arg in ipairs(args) do
		new_args[#new_args + 1] = arg
	end

	return new_args
end

cbox.hooks.Add("PreChatAddText", "cbox.timestamps", function(args)
	if not ENABLED:GetBool() then return end
	if not ALL_MESSAGES:GetBool() then return end

	return MakeTimestamp(args)
end)

cbox.hooks.Add("PrePlayerChat", "cbox.timestamps", function(args)
	if not ENABLED:GetBool() then return end
	if ALL_MESSAGES:GetBool() then return end

	return MakeTimestamp(args)
end)
