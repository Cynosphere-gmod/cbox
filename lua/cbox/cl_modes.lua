cbox.chatbox.AddMode("say", "Say", function(text)
	RunConsoleCommand("say", text)
end)

cbox.chatbox.AddMode("say_team", "Team", function(text)
	RunConsoleCommand("say_team", text)
end)

cbox.chatbox.AddMode("cmd", "Console", function(text)
	cbox.utils.RealmPrint(Format("Running command %q", text))

	if IsConCommandBlocked(text) then
		LocalPlayer():ChatPrint("Tried to run blocked command.")
		return
	end

	local function catch(err)
		LocalPlayer():ChatPrint("Failed to run command: " .. err)
	end

	xpcall(function() LocalPlayer():ConCommand(text) end, catch)
end)
