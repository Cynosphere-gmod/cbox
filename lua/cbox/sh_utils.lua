AddCSLuaFile()

local cbox = cbox

local MsgC = MsgC
local Color = Color
local ipairs = ipairs
local tonumber = tonumber
local tostring = tostring

local color_white = Color(255, 255, 255)
local color_black = Color(0, 0, 0)

local utils = {}
utils.colors = {
	BRAND  = Color(132, 206, 181),
	CLIENT = Color(230, 218, 112),
	SERVER = Color(143, 218, 230),
	SHARED = Color(93,  200, 92 ),
	ERROR  = Color(255, 90,  90 ),
}

---Parses a string into separate numbers
---@param str string Input color as "R G B" or "R G B A"
---@param alpha? boolean Whether to parse alpha or not (default false)
---@return number r
---@return number g
---@return number b
---@return number a
function utils.ParseColorString(str, alpha)
	alpha = alpha ~= nil and alpha or false

	local r, g, b = str:match("(%d+) (%d+) (%d+)")
	local a = 255
	if alpha then
		a = str:match("%d+ %d+ %d+ (%d+)")
	end

	return r, g, b, a
end

---Parses a string into a color
---@param str string Input color as "R G B" or "R G B A"
---@param alpha? boolean Whether to parse alpha or not (default false)
---@return Color
function utils.ParseColorStringToColor(str, alpha)
	local r, g, b, a = utils.ParseColorString(str, alpha)
	return Color(r, g, b, a)
end

---Prints to console with realm prefix
---@param ... any
function utils.RealmPrint(...)
	local args = {...}
	for i, arg in ipairs(args) do
		args[i] = tostring(arg)
	end

	MsgC(utils.colors.BRAND, "[cbox -> ")

	local realm, realm_color = "???", utils.colors.SHARED
	if CLIENT then
		realm = "CLIENT"
		realm_color = utils.colors.CLIENT
	elseif SERVER then
		realm = "SERVER"
		realm_color = utils.colors.SERVER
	end
	MsgC(realm_color, realm)

	MsgC(utils.colors.BRAND, "] ")

	MsgC(color_white, table.concat(args, " "), "\n")
end

---Prints to console with the "shared" prefix
---@param ... any
function utils.SharedPrint(...)
	local args = {...}
	for i, arg in ipairs(args) do
		args[i] = tostring(arg)
	end

	MsgC(utils.colors.BRAND, "[cbox -> ")

	MsgC(utils.colors.SHARED, "SHARED")

	MsgC(utils.colors.BRAND, "] ")

	MsgC(color_white, table.concat(args, " "), "\n")
end

---Prints to console with realm prefix, but in red
---@param ... any
function utils.RealmError(...)
	local args = {...}
	for i, arg in ipairs(args) do
		args[i] = tostring(arg)
	end

	MsgC(utils.colors.BRAND, "[cbox -> ")

	local realm, realm_color = "???", utils.colors.SHARED
	if CLIENT then
		realm = "CLIENT"
		realm_color = utils.colors.CLIENT
	elseif SERVER then
		realm = "SERVER"
		realm_color = utils.colors.SERVER
	end
	MsgC(realm_color, realm)

	MsgC(utils.colors.BRAND, "] ")

	MsgC(utils.colors.ERROR, table.concat(args, " "), "\n")
end

---Prints to console with the "shared" prefix, but in red
---@param ... any
function utils.SharedError(...)
	local args = {...}
	for i, arg in ipairs(args) do
		args[i] = tostring(arg)
	end

	MsgC(utils.colors.BRAND, "[cbox -> ")

	MsgC(utils.colors.SHARED, "SHARED")

	MsgC(utils.colors.BRAND, "] ")

	MsgC(utils.colors.ERROR, table.concat(args, " "), "\n")
end

---Gets a readable foreground color for a background color based on W3C standards
---@param col Color
---@return Color
function utils.GetReadableColor(col)
	return (col.r * 0.299 + col.g * 0.587 + col.b * 0.114) > 186 and color_black or color_white
end

cbox.utils = utils
